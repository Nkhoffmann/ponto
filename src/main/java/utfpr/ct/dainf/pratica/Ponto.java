package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;
    
    public Ponto() {
    x = y = z = 0;
    
    }
    
    public Ponto(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z; 
    }
    
    
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
    
    public double getZ() {
        return z;
    }
    
    public void setZ(double z) {
        this.z = z;
    }

    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }

    
    @Override
    public String toString() {
        return super.toString() + getNome() + "(" + x + "," + y + "," + z + ")";
    }

    
    
    public double getDist() {
        return Math.sqrt(Math.pow(x - x,2) + Math.pow(y - y, 2) + Math.pow(z - z, 2));
    }
    
    

}
