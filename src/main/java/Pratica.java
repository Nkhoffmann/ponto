/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Nikolas
 */


import utfpr.ct.dainf.if62c.pratica.PontoXY;
import utfpr.ct.dainf.if62c.pratica.PontoYZ;


public class Pratica {

    public static void main(String[] args) {
    

        PontoXY xy = new PontoXY(-3, 2);
        PontoYZ yz = new PontoYZ(0,2);
        
        System.out.println("Distancia = " + xy.yz.getDist());   
        
    }
    
}
